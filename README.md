# Welcome to Zamawiacz!

Zamawiacz - An chrome extension that allows you to share your order with your team on slack.
![Example.](https://i.ibb.co/BKxQmJL/zamawiacz.png)

# Setup

 1. git clone.
 2. Go to: [Chrome extensions tab](chrome://extensions/).
 3. Activate "developer mode" in top-right corner.
 4. Now press load unpacked and navigate to your cloned dir (zamawiacz/ext).

## Slack App setup

 1. Go to: [Slack Apps](https://api.slack.com/apps).
 2. Create new App - choose name e.g. "Zamawiacz", and workspace destination.
 3. Go to: [Webhooks tab](https://api.slack.com/apps/AMX85RQ5A/incoming-webhooks?).
 4. Create new Webhook for desired channel in your slack.
 5. Copy url and share with your friends on slack.

## Configuration
 1. Click zamawiacz icon in top-right corner next to the adres bar in chrome.
 2. Go to the "URL" tab and provide created webhook url. Save it.
 3. Go to the "Profil" and fill your slack data. Phone number for blik is optional.
 4. You are ready to go. Go to your fav. page, fill time of order and hit "Zamawiam".

![Addon popup.](https://i.ibb.co/PFXpcq8/zamawiacz.png)

## Pyszne.pl

Zamawiacz can inject quick action button and time chooser on any pyszne.pl sub store. Just go to the pyszne.pl and you'll find:

![It works the same way like form addon popup.](https://i.ibb.co/R9nB7Bg/zamawiacz.png)