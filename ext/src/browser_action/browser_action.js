$(function () {
    const alert = $('#number-alert');

    const profileOptions = $("#saver");
    const slackOptions = $("#saver-two");
    const orderOption = $("#sender");

    const nameField = $('#user-name');
    const numberField = $('#user-number');
    const slackUrlField = $('#slack-url');

    let name;
    let number;
    let slack;

    //On document loaded
    setVarsFromLocal();
    setHelpUrlView();

    //On order form chrome_action
    orderOption.click(function () {
        if (name !== "" && slack !== "") {

            let time = $('#time-input').val();

            if (time === "") {
                //Idle when time is not set;
                return;
            }

            //Post to slack
            chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                let url = tabs[0].url;

                let text;
                if (number !== "") {
                    text = "@" + name + " o " + time + " zamawiam z " + url + " ---> :slack: " + number;
                } else {
                    text = "@" + name + " o " + time + " zamawiam z " + url;
                }

                var payload = JSON.parse('{ "link_names": "1", "text": "' + text + '" }');
                var xhr = new XMLHttpRequest();
                xhr.open("POST", slack, true);
                xhr.setRequestHeader('Content-Type', 'text/html');
                xhr.send(JSON.stringify(payload));
            });

        } else {
            nameField.focus();
        }

    });

    //On user settings save
    slackOptions.click(function () {
        const url_val = slackUrlField.val();

        if (url_val !== "") {
            chrome.storage.sync.set({"zamurl": url_val}, function (data) {
                console.log('URL saved [name]');
            });
            $('#sender-form').submit();
            reloadPage();
        } else {

        }
    });

    //On slack settings save
    profileOptions.click(function () {
        const name_val = nameField.val();
        const number_val = numberField.val();

        if (phonenumber(number_val) || number_val === "") {

            chrome.storage.sync.set({"zamname": name_val}, function (data) {
                console.log('Settings saved [name]');
            });
            chrome.storage.sync.set({"zamnumb": number_val}, function (data) {
                console.log('Settings saved [number]');
            });

            setVarsFromLocal();

            reloadPage();

            $('#sender-form').submit();
        } else {
            alert.show();
        }

    });

    function reloadPage() {
        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            chrome.tabs.update(tabs[0].id, {url: tabs[0].url});
        });
    }


    function setVarsFromLocal() {
        alert.hide();
        setHelpUrlView();
        chrome.storage.sync.get('zamname', function (data) {
            nameField.val(data.zamname);
            name = data.zamname;
        });
        chrome.storage.sync.get('zamnumb', function (data) {
            numberField.val(data.zamnumb);
            number = data.zamnumb;
        });
        chrome.storage.sync.get('zamurl', function (data) {
            slackUrlField.val(data.zamurl);
            slack = data.zamurl;
        });
    }

    function setHelpUrlView() {
        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            let helpUrl = $('#reminder-url');
            console.log(tabs[0].hostname);
            console.log(tabs[0].url);
            console.log(tabs[0].title);

            helpUrl.text(tabs[0].title);
            helpUrl.attr("href", tabs[0].url);
        });
    }

    function phonenumber(text) {
        const regExp = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/;
        return !!text.match(regExp);
    }
});
