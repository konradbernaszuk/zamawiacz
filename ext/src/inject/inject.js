chrome.extension.sendMessage({}, function (response) {

    let name;
    let number;
    let slackUrl;

    getDataFromStorage();

    const readyStateCheckInterval = setInterval(function (newChild) {

        if (document.readyState === "complete") { // HERE AFTER PAGE LOADED

            injectHtml();

            document.addEventListener('click', function (event) {
                if (!event.target.matches('.injected-link-zamawiacz')) return;

                postDataToSlack();

                event.preventDefault();
            }, false);

            clearInterval(readyStateCheckInterval);
        }

        //-----------------------------------------------Functions------------------------------------------------------
        function injectHtml() {
            const button = document.createElement('a');
            button.setAttribute('class', 'injected-link-zamawiacz');
            button.setAttribute('style',
                'padding: 0px 10px 0px 10px;' +
                'border-left: 1px #ebebeb solid;' +
                'text-align: center;' +
                'color: #ce0000;');
            button.innerHTML = 'ZAMAWIAM';

            const timeField = document.createElement('input');
            timeField.setAttribute('id', 'injected-time-zamawiacz');
            timeField.setAttribute('type', 'time');
            timeField.setAttribute('value', '12:00');
            timeField.setAttribute('style',
                'border-left: 1px #ebebeb solid;' +
                'text-align: center;' +
                'color: #ce0000;' +
                'padding: 0px 0px 0px 10px');

            const target = document.getElementsByClassName("info-and-fav")[0];
            target.appendChild(timeField);
            target.appendChild(button);
        }

        function postDataToSlack() {
            let text;
            const url = window.location.href;
            let time = document.getElementById('injected-time-zamawiacz').value;

            if (name === "" || time === "" || slackUrl === "") {
                alert("Użyj ustawień Zamawiacza aby wypełnić obowiązkowe pola.");
                console.log("test");
            } else {

                if (number !== "") {
                    text = "@" + name + " o " + time + " zamawiam z " + url + " ---> :slack: " + number;
                } else {
                    text = "@" + name + " o " + time + " zamawiam z " + url;
                }

                var payload = JSON.parse('{ "link_names": "1", "text": "' + text + '" }');
                var xhr = new XMLHttpRequest();
                xhr.open("POST", slackUrl, true);
                xhr.setRequestHeader('Content-Type', 'text/html');
                xhr.send(JSON.stringify(payload));
            }
        }
    }, 10);

    //-----------------------------------------------Functions------------------------------------------------------
    function getDataFromStorage() {
        chrome.storage.sync.get('zamname', function (data) {
            name = data.zamname;
        });
        chrome.storage.sync.get('zamnumb', function (data) {
            number = data.zamnumb;
        });
        chrome.storage.sync.get('zamurl', function (data) {
            slackUrl = data.zamurl;
        });
    }
});